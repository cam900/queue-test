#include <list>
#include <iostream>

// list를 이용하여 queue 구현
template<class T>
class Queue_list
{
private:
	std::list<T> lst;

public:
	Queue_list() {}

	Queue_list& enqueue(const T& val) { lst.push_back(val); return *this; }
	Queue_list& dequeue()             { lst.pop_front(); return *this; }
	const T& front()                  { return lst.front(); }
	bool empty() const                { return lst.empty(); }
	std::size_t size()                { return lst.size(); }
	auto begin()                      { return lst.begin(); }
	auto end()                        { return lst.end(); }
};

// 배열로 큐 구현
template<class T>
class CircularQueue
{
private:
	T* arr;
	std::size_t front_idx = 0;  // 맨 앞 데이터의 인덱스
	std::size_t rear_idx = -1;  // 맨 마지막 데이터의 인텍스
	std::size_t count  = 0;     // 큐에 들어간 데이터의 갯수(크기)
	std::size_t capacity = 0;   // 큐의 용량 (그릇의 크기)
public:
	CircularQueue(std::size_t size)
		: front_idx(0)
		, rear_idx(-1)
		, count(0)
		, capacity(size)
	{
		arr = new T[size];
	}

	~CircularQueue()
	{
		delete[] arr;
	}

	bool empty() const { return count == 0; }
	bool full() const { return count == capacity; }
	std::size_t size() const { return count; }

	// 맨 마지막 위치에 데이터를 넣기
	CircularQueue& enqueue(const T& e)
	{
		if (full())
		{
			std::cout << "Overflow Error" << std::endl;
			return *this;
		}

		rear_idx = (rear_idx + 1) % capacity;
		arr[rear_idx] = e;
		count++;
		return *this;
	}
	// 맨 처음에 위치한 데이터 삭제
	CircularQueue& dequeue()
	{
		if (empty())
		{
			std::cout << "Underflow Error" << std::endl;
			return *this;
		}

		front_idx = (front_idx + 1) % capacity;
		count--;
		return *this;
	}

	const T& front() const { return arr[front_idx]; }
};

template<class T>
class Deque_list
{
private:
	std::list<T> lst;
public:
	Deque_list() {}

	Deque_list& push_front(const T& val) { lst.push_front(val); return *this; }
	Deque_list& push_back(const T& val)  { lst.push_back(val); return *this; }
	Deque_list& pop_front()              { lst.pop_front(); return *this; }
	Deque_list& pop_back()               { lst.pop_back(); return *this; }
	const T& front() const               { return lst.front(); }
	const T& back() const                { return lst.back(); }
	bool empty() const                   { return lst.empty(); }
	std::size_t size() const             { return lst.size(); }
};
