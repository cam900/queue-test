﻿#include "queue.hpp"
#include <iostream>
#include <queue>
#include <deque>

int main()
{
    Deque_list<int> d;
    d.push_back(10);
    d.push_back(20);
    d.push_back(30);
    d.push_back(40);
    d.push_front(50);
    d.push_front(60);
    d.pop_back();
    d.pop_front();
    while (!d.empty())
    {
        std::cout << d.front() << ", ";
        d.pop_front();
    }
    std::cout << std::endl;
}

/*
* // std::deque 예제
int main()
{
    std::deque<int> d = { 10,20,30,40 };

    d.push_front(50);
    d.push_back(60);

    for (int i = 0; i < d.size(); i++)
    {
        std::cout << d[i] << ", ";
    }
    std::cout << std::endl;
}
*/

/*
int main()
{
    CircularQueue<int> q(5);

    q.enqueue(10);
    q.enqueue(20);
    q.enqueue(30);
    q.enqueue(40);
    q.enqueue(50);
    q.dequeue();
    q.dequeue();
    q.enqueue(60);
    q.enqueue(70);

    q.enqueue(80); // 오버플로 에러

    while (!q.empty())
    {
        std::cout << q.front() << ", ";
        q.dequeue();
    }
    std::cout << std::endl;
}
*/
/*
// std 제공해주는 큐(queue)를 이용하여 예제 구현
int main()
{
    std::queue<int> q;
    q.push(10);
    q.push(20);
    q.push(30);
    q.pop();
    std::cout << q.front() << std::endl;
    q.push(40);
    while (!q.empty())
    {
        std::cout << q.front() << ",";
        q.pop();
    }
    std::cout << std::endl;
}
*/
/*
* // list를 이용하여 큐 구현후 예제풀이
int main()
{
    Queue_list<int> q;
    q.enqueue(10).enqueue(20).enqueue(30); // 10, 20, 30
    q.dequeue(); // 20, 30
    std::cout << q.front() << std::endl;
    q.enqueue(40);

    while (!q.empty())
    {
        auto& e = q.front();
        std::cout << e << ", ";
        q.dequeue();
    }
    std::cout << std::endl;
}
*/
